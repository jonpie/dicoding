package com.footballmatch.jhonson.submission2.main

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.example.kotlinmvpespresso.ui.splash.MainIdlingResource
import com.footballmatch.jhonson.submission2.R.id.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class TeamFragmentTest {
    var idlingResource: MainIdlingResource? = null

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        idlingResource = MainIdlingResource(2000)
        Espresso.registerIdlingResources(idlingResource)
    }

    @After
    fun tearDown() {
        Espresso.unregisterIdlingResources(idlingResource)
    }

    @Test
    fun testRecyclerViewTeamBehaviour() {
        onView(withId(bottom_navigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_team)).perform(click())
        onView(withId(listEvent))
                .check(matches(isDisplayed()))
        onView(withId(listEvent)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(2))
        onView(withId(listEvent)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(5, click()))
    }

}
