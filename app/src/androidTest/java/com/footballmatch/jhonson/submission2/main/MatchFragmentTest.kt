package com.footballmatch.jhonson.submission2.main

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.example.kotlinmvpespresso.ui.splash.MainIdlingResource
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.R.id.*
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class MatchFragmentTest {
    var idlingResource: MainIdlingResource? = null

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        idlingResource = MainIdlingResource(2000)
        Espresso.registerIdlingResources(idlingResource)
    }

    @After
    fun tearDown() {
        Espresso.unregisterIdlingResources(idlingResource)
    }

    @Test
    fun testRecyclerViewPrevMatchBehaviour() {
        onView(withId(bottom_navigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_match)).perform(click())
        val tabView = onView(
                allOf<View>(withContentDescription("Prev Match"),
                        childAtPosition(childAtPosition(withId(R.id.tab_mode_match), 0), 0),
                        isDisplayed()))
        tabView.perform(click())
        val _LinearLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_event),
                                childAtPosition(
                                        withClassName(`is`<String>("org.jetbrains.anko._RelativeLayout")),
                                        0)),
                        0),
                        isDisplayed()))
        _LinearLayout.perform(click())
    }

    @Test
    fun testRecyclerViewNextBehaviour() {
        onView(withId(bottom_navigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_match)).perform(click())
        val tabView = onView(
                allOf<View>(withContentDescription("Next Match"),
                        childAtPosition(childAtPosition(withId(R.id.tab_mode_match), 0), 1),
                        isDisplayed()))
        tabView.perform(click())

        val _LinearLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_event),
                                childAtPosition(
                                        withClassName(`is`("org.jetbrains.anko._RelativeLayout")),
                                        0)),
                        1),
                        isDisplayed()))
        _LinearLayout.perform(click())
        onView(withId(add_to_favorite))
                .check(matches(isDisplayed()))
        onView(withId(add_to_favorite)).perform(click())
    }

//    @Test
//    fun testRecyclerViewFavouriteBehaviour() {
//        onView(withId(bottom_navigation))
//                .check(matches(isDisplayed()))
//        onView(withId(navigation_favourite)).perform(click())
//        onView(withId(list_favourite))
//                .check(matches(isDisplayed()))
//        onView(withId(list_favourite)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(0))
//        onView(withId(list_favourite)).perform(
//                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
//        onView(withId(add_to_favorite))
//                .check(matches(isDisplayed()))
//        onView(withId(add_to_favorite)).perform(click())
//        pressBack()
//    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

}
