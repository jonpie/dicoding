package com.footballmatch.jhonson.submission2.main

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.example.kotlinmvpespresso.ui.splash.MainIdlingResource
import com.footballmatch.jhonson.submission2.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class SearchMatchFragmentTest {
    var idlingResource: MainIdlingResource? = null

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        idlingResource = MainIdlingResource(2000)
        Espresso.registerIdlingResources(idlingResource)
    }

    @After
    fun tearDown() {
        Espresso.unregisterIdlingResources(idlingResource)
    }

    @Test
    fun testRecyclerViewSearchMatchBehaviour() {
        onView(withId(R.id.bottom_navigation))
                .check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.navigation_match)).perform(click())

        val appCompatImageView = onView(
                allOf<View>(withId(R.id.search_button), withContentDescription("Search"),
                        childAtPosition(
                                allOf<View>(withId(R.id.search_bar),
                                        childAtPosition(
                                                withId(R.id.menu_search),
                                                0)),
                                1),
                        isDisplayed()))
        appCompatImageView.perform(click())

        val searchAutoComplete = onView(
                allOf<View>(withId(R.id.search_src_text),
                        childAtPosition(
                                allOf<View>(withId(R.id.search_plate),
                                        childAtPosition(
                                                withId(R.id.search_edit_frame),
                                                1)),
                                0),
                        isDisplayed()))
        searchAutoComplete.perform(replaceText("Man"), closeSoftKeyboard())

        val _LinearLayout = onView(
                allOf<View>(childAtPosition(
                        allOf<View>(withId(R.id.list_event),
                                childAtPosition(
                                        withClassName(`is`<String>("org.jetbrains.anko._RelativeLayout")),
                                        0)),
                        0),
                        isDisplayed()))
        _LinearLayout.perform(click())

        val actionMenuItemView = onView(
                allOf<View>(withId(R.id.add_to_favorite), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView.perform(click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

}
