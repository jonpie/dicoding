package com.footballmatch.jhonson.submission2.main

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.Espresso.pressBack
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.example.kotlinmvpespresso.ui.splash.MainIdlingResource
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.R.id.bottom_navigation
import com.footballmatch.jhonson.submission2.R.id.navigation_favourite
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class FavouriteMatchFragmentTest {
    var idlingResource: MainIdlingResource? = null

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        idlingResource = MainIdlingResource(2000)
        Espresso.registerIdlingResources(idlingResource)
    }

    @After
    fun tearDown() {
        Espresso.unregisterIdlingResources(idlingResource)
    }

    @Test
    fun testRecyclerViewFavMatchBehaviour() {
        onView(withId(bottom_navigation))
                .check(matches(isDisplayed()))
        onView(withId(R.id.navigation_match)).perform(click())
        val tabViewNext = onView(
                allOf<View>(withContentDescription("Next Match"),
                        childAtPosition(childAtPosition(withId(R.id.tab_mode_match), 0), 1),
                        isDisplayed()))
        tabViewNext.perform(click())

        val _LinearLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_event),
                                childAtPosition(
                                        withClassName(`is`("org.jetbrains.anko._RelativeLayout")),
                                        0)),
                        1),
                        isDisplayed()))
        _LinearLayout.perform(click())
        onView(withId(R.id.add_to_favorite))
                .check(matches(isDisplayed()))
        onView(withId(R.id.add_to_favorite)).perform(click())
        pressBack()
        onView(withId(bottom_navigation))
                .check(matches(isDisplayed()))
        onView(withId(navigation_favourite)).perform(click())
        val tabView2 = onView(
                allOf(withContentDescription("Matches"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tab_mode_match),
                                        0),
                                0),
                        isDisplayed()))
        tabView2.perform(click())

        val _LinearLayout2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.list_favourite),
                                childAtPosition(
                                        withClassName(`is`("org.jetbrains.anko._LinearLayout")),
                                        0)),
                        0),
                        isDisplayed()))
        _LinearLayout2.perform(click())

        val actionMenuItemView2 = onView(
                allOf(withId(R.id.add_to_favorite), withContentDescription("Favorites"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.action_bar),
                                        2),
                                0),
                        isDisplayed()))
        actionMenuItemView2.perform(click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

}
