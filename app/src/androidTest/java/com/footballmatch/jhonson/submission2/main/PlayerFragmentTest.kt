package com.footballmatch.jhonson.submission2.main

import android.support.test.espresso.Espresso
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.view.View
import android.view.ViewGroup
import com.example.kotlinmvpespresso.ui.splash.MainIdlingResource
import com.footballmatch.jhonson.submission2.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.allOf
import org.hamcrest.TypeSafeMatcher
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@Suppress("DEPRECATION")
@RunWith(AndroidJUnit4::class)
class PlayerFragmentTest {
    var idlingResource: MainIdlingResource? = null

    @Rule
    @JvmField
    var activityRule = ActivityTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        idlingResource = MainIdlingResource(2000)
        Espresso.registerIdlingResources(idlingResource)
    }

    @After
    fun tearDown() {
        Espresso.unregisterIdlingResources(idlingResource)
    }

    @Test
    fun testRecyclerViewPlayerBehaviour() {
        onView(withId(R.id.bottom_navigation))
                .check(ViewAssertions.matches(isDisplayed()))
        onView(withId(R.id.navigation_team)).perform(click())

        val spinner = onView(
                allOf<View>(withId(R.id.spinner),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.main_container),
                                        0),
                                0),
                        isDisplayed()))
        spinner.perform(click())

        val appCompatCheckedTextView = Espresso.onData(Matchers.anything())
                .inAdapterView(childAtPosition(
                        withClassName(`is`<String>("android.widget.PopupWindow\$PopupBackgroundView")),
                        0))
                .atPosition(5)
        appCompatCheckedTextView.perform(click())

        val _LinearLayout = onView(
                allOf<View>(childAtPosition(
                        allOf<View>(withId(R.id.listEvent),
                                childAtPosition(
                                        withClassName(`is`<String>("org.jetbrains.anko._RelativeLayout")),
                                        0)),
                        3),
                        isDisplayed()))
        _LinearLayout.perform(click())

        val tabView = onView(
                allOf<View>(withContentDescription("Player"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.tabLayout),
                                        0),
                                1),
                        isDisplayed()))
        tabView.perform(click())

        val _LinearLayout2 = onView(
                allOf<View>(childAtPosition(
                        allOf<View>(withId(R.id.listPlayer),
                                childAtPosition(
                                        withClassName(`is`<String>("org.jetbrains.anko._RelativeLayout")),
                                        0)),
                        3),
                        isDisplayed()))
        _LinearLayout2.perform(click())
    }

    private fun childAtPosition(
            parentMatcher: Matcher<View>, position: Int): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return (parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position))
            }
        }
    }

}
