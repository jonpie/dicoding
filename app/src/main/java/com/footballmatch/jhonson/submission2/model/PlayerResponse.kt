package com.footballmatch.jhonson.submission2.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlayerResponse(
        @SerializedName("player")
        val players: List<Player>) : Parcelable