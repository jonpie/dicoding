package com.footballmatch.jhonson.submission2.main.teams.detail

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.InputFilter
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.footballmatch.jhonson.submission2.R.id.*
import com.footballmatch.jhonson.submission2.model.Player
import kotlinx.android.extensions.LayoutContainer
import org.jetbrains.anko.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class PlayerAdapter(private val context: Context, private val players: List<Player>, private val listener: (Player) -> Unit)
    : RecyclerView.Adapter<PlayerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlayerViewHolder {
        return PlayerViewHolder(PlayerUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: PlayerViewHolder, position: Int) {
        holder.bindItem(players[position], listener)
    }

    override fun getItemCount(): Int = players.size

}

class PlayerUI : AnkoComponent<ViewGroup> {
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(16)
                orientation = LinearLayout.HORIZONTAL

                imageView {
                    id = player_badge
                }.lparams {
                    height = dip(50)
                    width = dip(50)
                }

                textView {
                    id = player_name
                    textSize = 16f
                    ellipsize = TextUtils.TruncateAt.END
                    singleLine = true
                    filters = arrayOf(InputFilter.LengthFilter(12))
                }.lparams {
                    margin = dip(15)
                }

                textView {
                    id = player_position
                    textSize = 11f
                    gravity = Gravity.CENTER
                }.lparams {
                    setMargins(0, dip(15), 0, dip(15))
                }
            }
        }
    }

}

class PlayerViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {

    private val playerBadge: ImageView = itemView.find(player_badge)
    private val playerName: TextView = itemView.find(player_name)
    private val playerPosition: TextView = itemView.find(player_position)

    fun bindItem(players: Player, listener: (Player) -> Unit) {
        Glide.with(containerView).load(players.strCutout).into(playerBadge)
        playerName.text = players.strPlayer
        playerPosition.text = players.strPosition
        itemView.onClick { listener(players) }
    }
}