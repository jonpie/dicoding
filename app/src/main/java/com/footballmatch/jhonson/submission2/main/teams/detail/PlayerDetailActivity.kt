package com.footballmatch.jhonson.submission2.main.teams.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.footballmatch.jhonson.submission2.model.Player
import org.jetbrains.anko.*

class PlayerDetailActivity : AppCompatActivity() {

    private lateinit var player: Player

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        player = intent.getParcelableExtra("playerBundle")
        if (supportActionBar != null) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.title = player.strPlayer
        }

        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            leftPadding = dip(16)
            rightPadding = dip(16)

            imageView {
                Glide.with(this@PlayerDetailActivity).load(player.strThumb).into(this)
            }.lparams {
                width = matchParent
                height = dip(250)
            }

            linearLayout {
                setPadding(0, dip(10), 0, 0)
                orientation = LinearLayout.HORIZONTAL
                textView {
                    textSize = 14f
                    gravity = Gravity.CENTER
                    text = "Weight(Kg)"
                }.lparams {
                    width = matchParent
                    weight = 0.5F
                }
                textView {
                    textSize = 14f
                    gravity = Gravity.CENTER
                    text = "Height(m)"
                }.lparams {
                    width = matchParent
                    weight = 0.5F
                }
            }

            linearLayout {
                setPadding(0, dip(10), 0, 0)
                orientation = LinearLayout.HORIZONTAL
                textView {
                    textSize = 24f
                    gravity = Gravity.CENTER
                    text = player.strWeight
                }.lparams {
                    width = matchParent
                    weight = 0.5F
                }
                textView {
                    textSize = 24f
                    gravity = Gravity.CENTER
                    text = player.strHeight
                }.lparams {
                    width = matchParent
                    weight = 0.5F
                }
            }

            textView {
                textSize = 14f
                gravity = Gravity.CENTER
                text = "Forward"
            }

            imageView {
                setBackgroundColor(resources.getColor(android.R.color.black))
            }.lparams {
                height = dip(1)
                width = matchParent
            }

            textView {
                textSize = 14f
                gravity = Gravity.START
                text = player.strDescriptionEN
            }

        }

    }
}
