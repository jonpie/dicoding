package com.footballmatch.jhonson.submission2.main.match

import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.api.TheSportApi
import com.footballmatch.jhonson.submission2.model.EventResponse
import com.footballmatch.jhonson.submission2.util.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class NextMatchPresenter(private val view: NextMatchView,
                         private val apiRepository: ApiRepository,
                         private val gson: Gson, private val context: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getNextEvent(eventID: String?) {
        view.showLoading()

        async(context.main){
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportApi.getNextEvent(eventID)),
                        EventResponse::class.java
                )
            }
            view.showNextEvent(data.await().events)
            view.hideLoading()
        }
    }

}

