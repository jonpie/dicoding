package com.footballmatch.jhonson.submission2.main.match

import com.footballmatch.jhonson.submission2.model.Event

interface MatchView {
    fun showLoading()
    fun hideLoading()
    fun showEvent(data: List<Event>)
}