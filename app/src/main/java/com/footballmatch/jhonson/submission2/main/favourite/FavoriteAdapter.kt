package com.footballmatch.jhonson.submission2.main.favourite

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.support.v7.widget.RecyclerView
import android.text.InputFilter
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.R.id.*
import com.footballmatch.jhonson.submission2.db.FavoriteMatch
import kotlinx.android.extensions.LayoutContainer
import org.jetbrains.anko.*

class FavouriteAdapter(private val favourite: List<FavoriteMatch>, private val listener: (FavoriteMatch) -> Unit)
    : RecyclerView.Adapter<FavouriteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        return FavouriteViewHolder(EventFavouriteUI().createView(AnkoContext.create(parent.context, parent)))
    }

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
        holder.bindItem(favourite[position], listener)
    }

    override fun getItemCount(): Int = favourite.size

}

@Suppress("DEPRECATION")
class EventFavouriteUI : AnkoComponent<ViewGroup> {
    @SuppressLint("SetTextI18n")
    override fun createView(ui: AnkoContext<ViewGroup>): View {
        return with(ui) {
            linearLayout {
                lparams(width = matchParent, height = wrapContent)
                padding = dip(4)
                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.VERTICAL
                    padding = dip(16)
                    setBackgroundColor(resources.getColor(R.color.colorWhite))
                    textView {
                        id = event_date
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                    }.lparams {
                        gravity = Gravity.CENTER
                        setMargins(0, 0, 0, dip(10))
                    }

                    linearLayout {
                        lparams(width = matchParent, height = wrapContent)
                        orientation = LinearLayout.HORIZONTAL
                        gravity = Gravity.CENTER

                        textView {
                            id = team_name_home
                            textSize = 16f
                            ellipsize = TextUtils.TruncateAt.END
                            singleLine = true
                            filters = arrayOf(InputFilter.LengthFilter(10))
                        }.lparams {
                            width = dip(80)
                            setMargins(0, 0, dip(10), 0)
                        }

                        textView {
                            id = team_score_home
                            textSize = 16f
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams {
                            setMargins(0, 0, dip(10), 0)
                        }

                        textView {
                            text = "vs"
                            textSize = 16f
                        }.lparams {
                            setMargins(0, 0, dip(10), 0)
                        }

                        textView {
                            id = team_score_away
                            textSize = 16f
                            typeface = Typeface.DEFAULT_BOLD
                        }.lparams {
                            setMargins(0, 0, dip(10), 0)
                        }

                        textView {
                            id = team_name_away
                            textSize = 16f
                            ellipsize = TextUtils.TruncateAt.END
                            singleLine = true
                            filters = arrayOf(InputFilter.LengthFilter(10))
                        }.lparams {
                            width = dip(80)
                            setMargins(dip(10), 0, 0, 0)
                        }
                    }
                }
            }
        }
    }

}

class FavouriteViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    private val teamNameHome: TextView = itemView.find(team_name_home)
    private val teamNameAway: TextView = itemView.find(team_name_away)
    private val teamScoreHome: TextView = itemView.find(team_score_home)
    private val teamScoreAway: TextView = itemView.find(team_score_away)
    private val eventDate: TextView = itemView.find(event_date)

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    fun bindItem(favourite: FavoriteMatch, listener: (FavoriteMatch) -> Unit) {
        teamNameHome.text = favourite.teamHome
        teamNameAway.text = favourite.teamAway
        teamScoreHome.text = favourite.teamScoreHome
        teamScoreAway.text = favourite.teamScoreAway

        eventDate.text = favourite.eventDate
        containerView.setOnClickListener { listener(favourite) }
    }
}