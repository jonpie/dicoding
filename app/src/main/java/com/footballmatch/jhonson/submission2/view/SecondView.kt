package com.footballmatch.jhonson.submission2.view

import com.footballmatch.jhonson.submission2.model.Event
import com.footballmatch.jhonson.submission2.model.Team

interface SecondView {
    fun showLoading()
    fun hideLoading()
    fun showEvent(event: List<Event>)
    fun showTeamHome(team: List<Team>)
    fun showTeamAway(team: List<Team>)
}