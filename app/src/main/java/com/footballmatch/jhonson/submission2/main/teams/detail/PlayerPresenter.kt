package com.footballmatch.jhonson.submission2.main.teams.detail

import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.api.TheSportApi
import com.footballmatch.jhonson.submission2.model.PlayerResponse
import com.footballmatch.jhonson.submission2.util.CoroutineContextProvider
import com.google.gson.Gson
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg

class PlayerPresenter(private val view: PlayerView,
                      private val apiRepository: ApiRepository,
                      private val gson: Gson, private val contextPool: CoroutineContextProvider = CoroutineContextProvider()) {

    fun getPlayer(teamId: String) {
        view.showLoading()
        async(contextPool.main) {
            val data = bg {
                gson.fromJson(apiRepository
                        .doRequest(TheSportApi.getPlayer(teamId)),
                        PlayerResponse::class.java
                )
            }
            view.showPlayer(data.await().players)
            view.hideLoading()
        }
    }
}