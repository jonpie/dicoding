package com.footballmatch.jhonson.submission2.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.widget.RelativeLayout
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.R.id.bottom_navigation
import com.footballmatch.jhonson.submission2.R.id.main_container
import com.footballmatch.jhonson.submission2.main.favourite.FavouriteFragment
import com.footballmatch.jhonson.submission2.main.match.MatchFragment
import com.footballmatch.jhonson.submission2.main.teams.TeamsFragment
import org.jetbrains.anko.design.bottomNavigationView
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.matchParent
import org.jetbrains.anko.relativeLayout

class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavigation: BottomNavigationView
    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = "Football Match Schedule"

        relativeLayout {
            lparams(width = matchParent, height = matchParent)

            frameLayout {
                lparams(width = matchParent, height = matchParent)
                id = main_container
            }.lparams {
                addRule(RelativeLayout.ABOVE, bottom_navigation)
            }

            bottomNavigation = bottomNavigationView {
                id = bottom_navigation
                inflateMenu(R.menu.navigation)
            }.lparams {
                addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
            }
            bottomNavigation.setOnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.navigation_team -> {
                        loadTeamsFragment(savedInstanceState)
                    }
                    R.id.navigation_match -> {
                        loadMatchesFragment(savedInstanceState)
                    }
                    R.id.navigation_favourite -> {
                        loadFavouriteFragment(savedInstanceState)
                    }
                }
                true
            }
            bottomNavigation.selectedItemId = R.id.navigation_team
        }
    }

    private fun loadTeamsFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(main_container, TeamsFragment(), TeamsFragment::class.simpleName)
                    .commit()
        }
    }

    private fun loadMatchesFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(main_container, MatchFragment(), MatchFragment::class.simpleName)
                    .commit()
        }
    }

    private fun loadFavouriteFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                    .beginTransaction()
                    .replace(main_container, FavouriteFragment(), FavouriteFragment::class.simpleName)
                    .commit()
        }
    }
}
