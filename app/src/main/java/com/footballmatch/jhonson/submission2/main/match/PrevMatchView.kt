package com.footballmatch.jhonson.submission2.main.match

import com.footballmatch.jhonson.submission2.model.Event

interface PrevMatchView {
    fun showLoading()
    fun hideLoading()
    fun showPastEvent(data: List<Event>)
}