package com.footballmatch.jhonson.submission2.util

import android.annotation.SuppressLint
import android.view.View
import java.text.SimpleDateFormat

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

@SuppressLint("SimpleDateFormat")
fun convertDate(dateString: String?): String {

    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val date = sdf.parse(dateString)
    val dayOfTheWeek = android.text.format.DateFormat.format("EEE", date) as String
    val stringMonth = android.text.format.DateFormat.format("MMM", date) as String
    val year = android.text.format.DateFormat.format("yyyy", date) as String
    val day = android.text.format.DateFormat.format("dd", date) as String

    return "$dayOfTheWeek, $day $stringMonth $year"
}