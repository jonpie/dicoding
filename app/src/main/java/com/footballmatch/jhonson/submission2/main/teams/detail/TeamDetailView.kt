package com.footballmatch.jhonson.submission2.main.teams.detail

import com.footballmatch.jhonson.submission2.model.Team

interface TeamDetailView {
    fun showLoading()
    fun hideLoading()
    fun showTeamDetail(data: List<Team>)
}