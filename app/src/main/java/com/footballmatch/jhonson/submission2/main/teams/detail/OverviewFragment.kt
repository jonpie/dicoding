package com.footballmatch.jhonson.submission2.main.teams.detail

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.ctx


class OverviewFragment : Fragment(), AnkoComponent<Context> {

    private lateinit var teamDescription: TextView

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        teamDescription.text = arguments?.getString("teamDescription")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = matchParent)
            orientation = LinearLayout.VERTICAL
            leftPadding = dip(16)
            rightPadding = dip(16)
            teamDescription = textView().lparams {
                topMargin = dip(20)
            }
        }
    }

}
