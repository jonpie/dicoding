package com.footballmatch.jhonson.submission2.model


data class TeamResponse(
        val teams: List<Team>)