package com.footballmatch.jhonson.submission2.main.teams

import com.footballmatch.jhonson.submission2.model.Team

interface TeamsView {
    fun showLoading()
    fun hideLoading()
    fun showTeamList(data: List<Team>)
    fun showTeam(team: List<Team>)
}