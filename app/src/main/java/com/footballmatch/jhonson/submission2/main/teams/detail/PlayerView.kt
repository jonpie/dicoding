package com.footballmatch.jhonson.submission2.main.teams.detail

import com.footballmatch.jhonson.submission2.model.Player

interface PlayerView {
    fun showLoading()
    fun hideLoading()
    fun showPlayer(data: List<Player>)
}