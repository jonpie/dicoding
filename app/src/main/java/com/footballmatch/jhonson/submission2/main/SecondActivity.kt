package com.footballmatch.jhonson.submission2.main

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import com.bumptech.glide.Glide
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.R.drawable.ic_add_to_favorites
import com.footballmatch.jhonson.submission2.R.drawable.ic_added_to_favorites
import com.footballmatch.jhonson.submission2.R.id.add_to_favorite
import com.footballmatch.jhonson.submission2.R.menu.detail_menu
import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.db.FavoriteMatch
import com.footballmatch.jhonson.submission2.db.database
import com.footballmatch.jhonson.submission2.model.Event
import com.footballmatch.jhonson.submission2.model.Team
import com.footballmatch.jhonson.submission2.util.convertDate
import com.footballmatch.jhonson.submission2.util.invisible
import com.footballmatch.jhonson.submission2.util.visible
import com.footballmatch.jhonson.submission2.view.SecondView
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.jetbrains.anko.design.snackbar

@Suppress("DEPRECATION")
class SecondActivity : AppCompatActivity(), SecondView {

    private lateinit var presenter: SecondPresenter
    private lateinit var progressBar: ProgressBar
    private lateinit var ivHome: ImageView
    private lateinit var ivAway: ImageView
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private lateinit var event: Event
    private lateinit var favourite: FavoriteMatch
    private lateinit var scrollView: ScrollView

    private lateinit var tvDate: TextView
    private lateinit var tvNameHome: TextView
    private lateinit var tvNameAway: TextView
    private lateinit var tvScoreHome: TextView
    private lateinit var tvScoreAway: TextView
    private lateinit var tvNameHomeForm: TextView
    private lateinit var tvNameAwayForm: TextView
    private lateinit var tvNameHomeGoal: TextView
    private lateinit var tvNameAwayGoal: TextView
    private lateinit var tvNameHomeShot: TextView
    private lateinit var tvNameAwayShot: TextView
    private lateinit var tvNameHomeGoalKeeper: TextView
    private lateinit var tvNameAwayGoalKeeper: TextView
    private lateinit var tvNameHomeDefense: TextView
    private lateinit var tvNameAwayDefense: TextView
    private lateinit var tvNameHomeMidfield: TextView
    private lateinit var tvNameAwayMidfield: TextView
    private lateinit var tvNameHomeForward: TextView
    private lateinit var tvNameAwayForward: TextView
    private lateinit var tvNameHomeSub: TextView
    private lateinit var tvNameAwaySub: TextView

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = "Match Detail"

        scrollView = scrollView {
            verticalLayout {
                lparams(width = matchParent, height = wrapContent)
                orientation = LinearLayout.VERTICAL
                padding = dip(20)

                relativeLayout {
                    lparams(width = matchParent, height = matchParent)
                    progressBar = progressBar {
                    }.lparams {
                        centerInParent()
                    }
                }

                tvDate = textView {
                    textSize = 14f
                    setTextColor(resources.getColor(R.color.colorPrimary))
                }.lparams {
                    gravity = Gravity.CENTER
                    setMargins(0, 0, 0, dip(10))
                }

                linearLayout {
                    lparams(width = matchParent, height = wrapContent)
                    orientation = LinearLayout.HORIZONTAL
                    gravity = Gravity.CENTER

                    linearLayout {
                        lparams(width = wrapContent, height = wrapContent)
                        orientation = LinearLayout.VERTICAL
                        ivHome = imageView {
                            padding = dip(10)
                        }.lparams {
                            height = dip(80)
                            width = dip(80)
                            gravity = Gravity.CENTER
                        }
                        tvNameHome = textView {
                            textSize = 14f
                            setTextColor(resources.getColor(R.color.colorPrimary))
                        }.lparams {
                            gravity = Gravity.CENTER_VERTICAL
                            width = matchParent
                            height = wrapContent
                        }
                        tvNameHomeForm = textView {
                            textSize = 14f
                            setTextColor(resources.getColor(R.color.colorPrimary))
                        }.lparams {
                            gravity = Gravity.CENTER_VERTICAL
                            width = matchParent
                            height = wrapContent
                            setMargins(0, dip(10), 0, 0)
                        }
                    }

                    tvScoreHome = textView {
                        textSize = 24f
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams {
                        setMargins(dip(10), 0, dip(10), 0)
                    }

                    textView {
                        text = "vs"
                        textSize = 16f
                    }.lparams {
                        setMargins(dip(10), 0, dip(10), 0)
                    }
                    tvScoreAway = textView {
                        textSize = 24f
                        typeface = Typeface.DEFAULT_BOLD
                    }.lparams {
                        setMargins(dip(10), 0, dip(10), 0)
                    }

                    linearLayout {
                        lparams(width = wrapContent, height = wrapContent)
                        orientation = LinearLayout.VERTICAL
                        ivAway = imageView {
                            padding = dip(10)
                        }.lparams {
                            height = dip(80)
                            width = dip(80)
                            gravity = Gravity.CENTER
                        }
                        tvNameAway = textView {
                            textSize = 14f
                            setTextColor(resources.getColor(R.color.colorPrimary))
                        }.lparams {
                            gravity = Gravity.CENTER_VERTICAL
                            width = matchParent
                            height = wrapContent
                        }
                        tvNameAwayForm = textView {
                            textSize = 14f
                            setTextColor(resources.getColor(R.color.colorPrimary))
                        }.lparams {
                            gravity = Gravity.CENTER_VERTICAL
                            width = matchParent
                            height = wrapContent
                            setMargins(0, dip(10), 0, 0)
                        }
                    }
                }

                imageView {
                    setBackgroundColor(resources.getColor(android.R.color.black))
                }.lparams {
                    height = dip(1)
                    width = matchParent
                    setMargins(0, dip(10), 0, dip(10))
                }

                linearLayout {
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeGoal = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Goals"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwayGoal = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }

                linearLayout {
                    setPadding(0, dip(10), 0, 0)
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeShot = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Shots"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwayShot = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }

                imageView {
                    setBackgroundColor(resources.getColor(android.R.color.black))
                }.lparams {
                    height = dip(1)
                    width = matchParent
                    setMargins(0, dip(10), 0, dip(10))
                }

                textView {
                    text = "Lineups"
                    textSize = 14f
                    gravity = Gravity.TOP + Gravity.CENTER
                }

                linearLayout {
                    setPadding(0, dip(10), 0, 0)
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeGoalKeeper = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Goal Keeper"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwayGoalKeeper = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }

                linearLayout {
                    setPadding(0, dip(10), 0, 0)
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeDefense = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Defense"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwayDefense = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }

                linearLayout {
                    setPadding(0, dip(10), 0, 0)
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeMidfield = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Midfield"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwayMidfield = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }

                linearLayout {
                    setPadding(0, dip(10), 0, 0)
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeForward = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Forward"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwayForward = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }

                linearLayout {
                    setPadding(0, dip(10), 0, 0)
                    orientation = LinearLayout.HORIZONTAL
                    tvNameHomeSub = textView {
                        textSize = 14f
                        gravity = Gravity.START
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                    textView {
                        text = "Subtitute"
                        textSize = 14f
                        setTextColor(resources.getColor(R.color.colorPrimary))
                        gravity = Gravity.TOP + Gravity.CENTER
                    }.lparams {
                        weight = 0.6F
                    }

                    tvNameAwaySub = textView {
                        textSize = 14f
                        gravity = Gravity.END
                    }.lparams {
                        width = dip(70)
                        weight = 0.2F
                    }
                }
            }
        }

        val request = ApiRepository()
        val gson = Gson()
        presenter = SecondPresenter(this, request, gson)

        val isEvent: Boolean = intent?.extras?.getBoolean("isEvent") ?: false
        if (isEvent) {
            event = intent.getParcelableExtra("eventBundle")
            setViewLayout(event)
            favoriteState(event.idEvent.toString())
        } else {
            favourite = intent.getParcelableExtra("eventBundle")
            favoriteState(favourite.id.toString())
            presenter.getEventDetails(favourite.id.toString())
        }
    }

    private fun favoriteState(eventID: String) {
        doAsync {
            database.use {
                val result = select(FavoriteMatch.TABLE_FAVORITE_MATCH)
                        .whereArgs("(ID_EVENT = {id})",
                                "id" to eventID)
                val favorite = result.parseList(classParser<FavoriteMatch>())
                if (!favorite.isEmpty()) isFavorite = true
            }
        }
    }

    private fun addToFavorite(event: Event) {
        doAsync {
            database.use {
                insert(FavoriteMatch.TABLE_FAVORITE_MATCH,
                        FavoriteMatch.ID_EVENT to event.idEvent,
                        FavoriteMatch.EVENT_TEAM_HOME to event.strHomeTeam,
                        FavoriteMatch.EVENT_TEAM_AWAY to event.strAwayTeam,
                        FavoriteMatch.EVENT_TEAM_SCORE_HOME to event.intHomeScore,
                        FavoriteMatch.EVENT_TEAM_SCORE_AWAY to event.intAwayScore,
                        FavoriteMatch.EVENT_DATE to convertDate(event.dateEvent))
            }
            uiThread {
                snackbar(scrollView, "Added to favorite").show()
            }
        }
    }

    private fun removeFromFavorite(eventID: String) {
        doAsync {
            database.use {
                delete(FavoriteMatch.TABLE_FAVORITE_MATCH, "(ID_EVENT = {id})",
                        "id" to eventID)
            }
            uiThread {
                snackbar(scrollView, "Removed to favorite").show()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(detail_menu, menu)
        menuItem = menu
        setFavorite()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                val intent = Intent()
                setResult(Activity.RESULT_OK, intent)
                onBackPressed()
                true
            }
            add_to_favorite -> {
                if (isFavorite) event.idEvent?.let { removeFromFavorite(it) } else addToFavorite(event)

                isFavorite = !isFavorite
                setFavorite()

                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showTeamHome(team: List<Team>) {
        if (team.isNotEmpty()) Glide.with(this@SecondActivity).load(team[0].teamBadge).into(ivHome)
    }

    override fun showTeamAway(team: List<Team>) {
        if (team.isNotEmpty()) Glide.with(this@SecondActivity).load(team[0].teamBadge).into(ivAway)
    }

    private fun setFavorite() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_added_to_favorites)
        else
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, ic_add_to_favorites)
    }

    override fun showEvent(event: List<Event>) {
        this.event = event[0]
        setViewLayout(event[0])
    }

    private fun setViewLayout(event: Event) {
        presenter.getTeamHome(event.strHomeTeam)
        presenter.getTeamAway(event.strAwayTeam)

        tvDate.text = convertDate(event.dateEvent)
        tvNameHome.text = event.strHomeTeam
        tvNameAway.text = event.strAwayTeam
        tvScoreHome.text = event.intHomeScore
        tvScoreAway.text = event.intAwayScore
        tvNameHomeForm.text = event.strHomeFormation
        tvNameAwayForm.text = event.strAwayFormation
        tvNameHomeGoal.text = event.strHomeGoalDetails
        tvNameAwayGoal.text = event.strAwayGoalDetails
        tvNameHomeShot.text = event.intHomeShots ?: ""
        tvNameAwayShot.text = event.intAwayShots ?: ""
        tvNameHomeGoalKeeper.text = event.strHomeLineupGoalkeeper ?: ""
        tvNameAwayGoalKeeper.text = event.strAwayLineupGoalkeeper ?: ""
        tvNameHomeDefense.text = event.strHomeLineupDefense ?: ""
        tvNameAwayDefense.text = event.strAwayLineupDefense ?: ""
        tvNameHomeMidfield.text = event.strHomeLineupMidfield ?: ""
        tvNameAwayMidfield.text = event.strAwayLineupMidfield ?: ""
        tvNameHomeForward.text = event.strHomeLineupForward ?: ""
        tvNameAwayForward.text = event.strAwayLineupForward ?: ""
        tvNameHomeSub.text = event.strHomeLineupSubstitutes ?: ""
        tvNameAwaySub.text = event.strAwayLineupSubstitutes ?: ""
    }
}
