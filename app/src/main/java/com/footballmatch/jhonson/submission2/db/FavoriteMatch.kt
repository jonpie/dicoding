package com.footballmatch.jhonson.submission2.db

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FavoriteMatch(val id: Long?, val teamHome: String?, val teamAway: String?,
                         val teamScoreHome: String?, val teamScoreAway: String?, val eventDate: String?) : Parcelable {

    companion object {
        const val TABLE_FAVORITE_MATCH: String = "TABLE_FAVORITE_MATCH"
        const val ID_EVENT: String = "ID_EVENT"
        const val EVENT_TEAM_HOME: String = "TEAM_HOME"
        const val EVENT_TEAM_AWAY: String = "TEAM_AWAY"
        const val EVENT_TEAM_SCORE_HOME: String = "SCORE_HOME"
        const val EVENT_TEAM_SCORE_AWAY: String = "SCORE_AWAY"
        const val EVENT_DATE: String = "EVENT_DATE"
    }
}