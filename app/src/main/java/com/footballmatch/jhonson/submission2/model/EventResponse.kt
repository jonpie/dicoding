package com.footballmatch.jhonson.submission2.model


data class EventResponse(
        val events: List<Event>)