package com.footballmatch.jhonson.submission2.main.match

import com.footballmatch.jhonson.submission2.model.Event

interface NextMatchView {
    fun showLoading()
    fun hideLoading()
    fun showNextEvent(data: List<Event>)
}