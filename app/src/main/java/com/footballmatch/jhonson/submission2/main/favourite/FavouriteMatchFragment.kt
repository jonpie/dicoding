package com.footballmatch.jhonson.submission2.main.favourite

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.db.FavoriteMatch
import com.footballmatch.jhonson.submission2.db.database
import com.footballmatch.jhonson.submission2.main.MainActivity
import com.footballmatch.jhonson.submission2.main.SecondActivity
import org.jetbrains.anko.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx


class FavouriteMatchFragment : Fragment(), AnkoComponent<Context> {

    private var favoriteMatches: MutableList<FavoriteMatch> = mutableListOf()
    private lateinit var favouriteAdapter: FavouriteAdapter
    private lateinit var listFavourite: RecyclerView

    override fun onResume() {
        super.onResume()
        showFavorite()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        favouriteAdapter = FavouriteAdapter(favoriteMatches) {
            (activity as MainActivity).startActivityForResult<SecondActivity>(1, "isEvent" to false, "eventBundle" to it)
        }
        listFavourite.adapter = favouriteAdapter
        showFavorite()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        linearLayout {
            lparams(width = matchParent, height = wrapContent)
            topPadding = dip(16)
            leftPadding = dip(16)
            rightPadding = dip(16)

            listFavourite = recyclerView {
                id = R.id.list_favourite
                lparams(width = matchParent, height = wrapContent)
                layoutManager = LinearLayoutManager(ctx)
            }
        }
    }

    private fun showFavorite() {
        doAsync {
            (activity as MainActivity).database.use {
                val result = select(FavoriteMatch.TABLE_FAVORITE_MATCH)
                val favorite = result.parseList(classParser<FavoriteMatch>())
                uiThread {
                    favoriteMatches.clear()
                    favoriteMatches.addAll(favorite)
                    favouriteAdapter.notifyDataSetChanged()
                }
            }
        }
    }

}
