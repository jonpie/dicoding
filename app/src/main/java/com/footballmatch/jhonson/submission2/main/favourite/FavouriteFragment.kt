package com.footballmatch.jhonson.submission2.main.favourite

import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.footballmatch.jhonson.submission2.R
import org.jetbrains.anko.*
import org.jetbrains.anko.design.themedTabLayout
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.viewPager
import java.util.*


class FavouriteFragment : Fragment(), AnkoComponent<Context> {

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        relativeLayout {
            lparams(width = matchParent, height = matchParent)
            tabLayout = themedTabLayout(R.style.tabLayout) {
                lparams(width = matchParent, height = wrapContent)
                id = R.id.tab_mode_match
                tabGravity = Gravity.FILL
                tabMode = TabLayout.MODE_FIXED
                background = ContextCompat.getDrawable(context, R.color.colorPrimaryDark)
            }

            viewPager = viewPager {
                id = R.id.view_pager_match
            }.lparams {
                width = matchParent
                height = matchParent
                addRule(RelativeLayout.BELOW, R.id.tab_mode_match)
            }
        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val vpAdapter = ViewPagerAdapter(childFragmentManager)
        val fragmentMatch = FavouriteMatchFragment()
        vpAdapter.addFragment(fragmentMatch, getString(R.string.match))
        val fragmentTeam = FavoriteTeamsFragment()
        vpAdapter.addFragment(fragmentTeam, getString(R.string.teams))
        viewPager.adapter = vpAdapter
    }

    private class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

}
