package com.footballmatch.jhonson.submission2.model

import com.google.gson.annotations.SerializedName


data class SearchEventResponse(
        @SerializedName("event")
        val events: List<Event>)