package com.footballmatch.jhonson.submission2.main.match

import android.app.SearchManager
import android.content.Context
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.footballmatch.jhonson.submission2.R
import com.footballmatch.jhonson.submission2.R.id.tab_mode_match
import com.footballmatch.jhonson.submission2.R.id.view_pager_match
import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.main.MainActivity
import com.footballmatch.jhonson.submission2.main.SecondActivity
import com.footballmatch.jhonson.submission2.model.Event
import com.footballmatch.jhonson.submission2.util.invisible
import com.footballmatch.jhonson.submission2.util.visible
import com.google.gson.Gson
import org.jetbrains.anko.*
import org.jetbrains.anko.design.themedTabLayout
import org.jetbrains.anko.recyclerview.v7.recyclerView
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.viewPager
import java.util.*


class MatchFragment : Fragment(), AnkoComponent<Context>, MatchView {

    private var events: MutableList<Event> = mutableListOf()
    private lateinit var presenter: MatchPresenter
    private lateinit var matchAdapter: MatchAdapter
    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var searchView: SearchView
    private lateinit var queryTextListener: SearchView.OnQueryTextListener
    private lateinit var searchViewLayout: RelativeLayout
    private lateinit var viewPagerLayout: RelativeLayout
    private lateinit var listEvents: RecyclerView
    private lateinit var progressBar: ProgressBar

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewPager(viewPager)
        tabLayout.setupWithViewPager(viewPager)

        matchAdapter = MatchAdapter(events) {
            (activity as MainActivity).startActivity<SecondActivity>("isEvent" to true, "eventBundle" to it)
        }
        listEvents.adapter = matchAdapter
        val request = ApiRepository()
        val gson = Gson()
        presenter = MatchPresenter(this, request, gson)
        hideLoading()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return createView(AnkoContext.create(ctx))
    }

    override fun createView(ui: AnkoContext<Context>): View = with(ui) {
        relativeLayout {
            lparams(width = matchParent, height = matchParent)

            searchViewLayout = relativeLayout {
                lparams(width = matchParent, height = matchParent)

                listEvents = recyclerView {
                    id = R.id.list_event
                    lparams(width = matchParent, height = wrapContent)
                    layoutManager = LinearLayoutManager(ctx)
                }

                progressBar = progressBar {
                }.lparams {
                    centerHorizontally()
                }
            }

            viewPagerLayout = relativeLayout {
                lparams(width = matchParent, height = matchParent)

                tabLayout = themedTabLayout(R.style.tabLayout) {
                    lparams(width = matchParent, height = wrapContent)
                    id = tab_mode_match
                    tabGravity = Gravity.FILL
                    tabMode = TabLayout.MODE_FIXED
                    background = ContextCompat.getDrawable(context, R.color.colorPrimaryDark)
                }

                viewPager = viewPager {
                    id = view_pager_match
                }.lparams {
                    width = matchParent
                    height = matchParent
                    addRule(RelativeLayout.BELOW, tab_mode_match)
                }
            }


        }
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val vpAdapter = ViewPagerAdapter(childFragmentManager)
        val fragmentPrev = PrevMatchFragment()
        vpAdapter.addFragment(fragmentPrev, getString(R.string.prev_match))
        val fragmentNext = NextMatchFragment()
        vpAdapter.addFragment(fragmentNext, getString(R.string.next_match))
        viewPager.adapter = vpAdapter
    }

    private class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.menu_search)
        val searchManager = (activity as MainActivity).getSystemService(Context.SEARCH_SERVICE) as SearchManager
        if (searchItem != null) {
            searchView = searchItem.actionView as SearchView
        }
        searchView.setSearchableInfo(searchManager.getSearchableInfo((activity as MainActivity).componentName))
        queryTextListener = object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
                if (newText.isNotEmpty()) {
                    hideViewPager()
                    presenter.getSearchEvent(newText)
                } else {
                    showViewPager()
                }
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                if (query.isNotEmpty()) {
                    hideViewPager()
                    presenter.getSearchEvent(query)
                } else {
                    showViewPager()
                }
                return true
            }
        }
        searchView.setOnQueryTextListener(queryTextListener)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun showViewPager() {
        viewPagerLayout.visible()
        searchViewLayout.invisible()
    }

    private fun hideViewPager() {
        viewPagerLayout.invisible()
        searchViewLayout.visible()
    }

    override fun showLoading() {
        progressBar.visible()
    }

    override fun hideLoading() {
        progressBar.invisible()
    }

    override fun showEvent(data: List<Event>) {
        events.clear()
        events.addAll(data)
        matchAdapter.notifyDataSetChanged()
    }

}
