package com.footballmatch.jhonson.submission2.main.teams.detail

import com.footballmatch.jhonson.submission2.TestContextProvider
import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.api.TheSportApi
import com.footballmatch.jhonson.submission2.model.Player
import com.footballmatch.jhonson.submission2.model.PlayerResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by root on 3/1/18.
 */
class PlayerPresenterTest {
    @Mock
    private
    lateinit var view: PlayerView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: PlayerPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = PlayerPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun testGetTeamDetail() {
        val teams: MutableList<Player> = mutableListOf()
        val response = PlayerResponse(teams)
        val teamID = "133604"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportApi.getPlayer(teamID)),
                PlayerResponse::class.java
        )).thenReturn(response)

        presenter.getPlayer(teamID)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showPlayer(teams)
        Mockito.verify(view).hideLoading()
    }

}