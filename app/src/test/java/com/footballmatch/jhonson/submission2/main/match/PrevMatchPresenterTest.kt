package com.footballmatch.jhonson.submission2.main.match

import com.footballmatch.jhonson.submission2.TestContextProvider
import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.api.TheSportApi
import com.footballmatch.jhonson.submission2.model.Event
import com.footballmatch.jhonson.submission2.model.EventResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class PrevMatchPresenterTest {
    @Mock
    private
    lateinit var view: PrevMatchView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: PrevMatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = PrevMatchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun testGetPastEvent() {
        val events: MutableList<Event> = mutableListOf()
        val response = EventResponse(events)
        val league = "4328"

        `when`(gson.fromJson(apiRepository
                .doRequest(TheSportApi.getPastEvent(league)),
                EventResponse::class.java
        )).thenReturn(response)

        presenter.getPastEvent(league)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showPastEvent(events)
        Mockito.verify(view).hideLoading()
    }

}