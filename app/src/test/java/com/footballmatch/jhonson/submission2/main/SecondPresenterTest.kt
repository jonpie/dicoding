package com.footballmatch.jhonson.submission2.main

import com.footballmatch.jhonson.submission2.TestContextProvider
import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.api.TheSportApi
import com.footballmatch.jhonson.submission2.model.Event
import com.footballmatch.jhonson.submission2.model.EventResponse
import com.footballmatch.jhonson.submission2.model.Team
import com.footballmatch.jhonson.submission2.model.TeamResponse
import com.footballmatch.jhonson.submission2.view.SecondView
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SecondPresenterTest {
    @Mock
    private
    lateinit var view: SecondView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: SecondPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = SecondPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun testTeamHome() {
        val team: MutableList<Team> = mutableListOf()
        val response = TeamResponse(team)
        val teamName = "Man%20United"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportApi.getTeamName(teamName)),
                TeamResponse::class.java
        )).thenReturn(response)

        presenter.getTeamHome(teamName)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showTeamHome(team)
        Mockito.verify(view).hideLoading()
    }

    @Test
    fun testTeamAway() {
        val team: MutableList<Team> = mutableListOf()
        val response = TeamResponse(team)
        val teamName = "Tottenham"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportApi.getTeamName(teamName)),
                TeamResponse::class.java
        )).thenReturn(response)

        presenter.getTeamAway(teamName)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showTeamAway(team)
        Mockito.verify(view).hideLoading()
    }

    @Test
    fun testEventDetail() {
        val events: MutableList<Event> = mutableListOf()
        val response = EventResponse(events)
        val eventID = "576491"

        Mockito.`when`(gson.fromJson(apiRepository
                .doRequest(TheSportApi.getEventDetails(eventID)),
                EventResponse::class.java
        )).thenReturn(response)

        presenter.getEventDetails(eventID)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showEvent(events)
        Mockito.verify(view).hideLoading()
    }

}