package com.footballmatch.jhonson.submission2.main.match

import com.footballmatch.jhonson.submission2.TestContextProvider
import com.footballmatch.jhonson.submission2.api.ApiRepository
import com.footballmatch.jhonson.submission2.api.TheSportApi
import com.footballmatch.jhonson.submission2.model.Event
import com.footballmatch.jhonson.submission2.model.SearchEventResponse
import com.google.gson.Gson
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations

class MatchPresenterTest {
    @Mock
    private
    lateinit var view: MatchView

    @Mock
    private
    lateinit var gson: Gson

    @Mock
    private
    lateinit var apiRepository: ApiRepository

    private lateinit var presenter: MatchPresenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MatchPresenter(view, apiRepository, gson, TestContextProvider())
    }

    @Test
    fun testSearchEvent() {
        val events: MutableList<Event> = mutableListOf()
        val response = SearchEventResponse(events)
        val league = "Man"

        `when`(gson.fromJson(apiRepository
                .doRequest(TheSportApi.getPastEvent(league)),
                SearchEventResponse::class.java
        )).thenReturn(response)

        presenter.getSearchEvent(league)

        Mockito.verify(view).showLoading()
        Mockito.verify(view).showEvent(events)
        Mockito.verify(view).hideLoading()
    }

}